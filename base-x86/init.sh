#!/bin/bash
# Setup and download any dependencies that are missing using the manifest

FORGE=$(cat /opt/manifest | jq -r .forge)
MOD=$(cat /opt/manifest | jq -r .mod | grep -v null)
LEVEL=$(cat /opt/manifest | jq -r .level | grep -v null)

# Download Forge, required parameter
if [ ! -f /opt/server/minecraft_server.jar ]; then
	echo "Downloading Forge..."
	(cd /opt/server && \
		curl --remote-name --progress-bar $FORGE && \
		java -jar forge-*-installer.jar --installServer && \
		rm forge-*-installer.jar && \
		mv forge-*-universal.jar minecraft_server.jar)
fi

# Install Mod, optional
if [  -n "$MOD" ] && [ -z "$(ls /opt/server/mods)" ]; then
	echo "Downloading Mod..."
	(cd /opt/server/mods && curl --remote-name --progress-bar $MOD)
fi

# Install the level, optional
if [ -n "$LEVEL" ] && [ -z "$(ls /opt/server/world)" ]; then
	echo "Downloading level..."
	(cd /opt/server/world && curl --remote-name --progress-bar $LEVEL)
	echo "Installing the level..."
	(cd /opt/server/world && \
		ZIP=$(ls *.zip) && \
		unzip $ZIP && \
		rm $ZIP && \
		f=(./*) && \
		mv ./*/* . && \
		rmdir "${f[@]}")
fi
