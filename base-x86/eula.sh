#!/bin/bash

if [ "$EULA" != "true" ]; then
	echo "Cannot start server until EULA has been accepted"
	exit -1
fi

echo "eula=true" > /opt/server/eula.txt
exit 0