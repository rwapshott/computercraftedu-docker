#!/bin/bash

# Minecraft Server 1.7.10
SERVER=minecraft_server.1.7.10.jar
[ ! -f "$SERVER" ] && curl -s https://launcher.mojang.com/v1/objects/952438ac4e01b4d115c5fc38f891710c4941df29/server.jar -o $SERVER

# Forge 1.7.10
FORGE=forge.jar
[ ! -f $FORGE ] && curl -s https://files.minecraftforge.net/maven/net/minecraftforge/forge/1.7.10-10.13.4.1614-1.7.10/forge-1.7.10-10.13.4.1614-1.7.10-installer.jar -o $FORGE

# ComputerCraftEdu Mod 1.74
MOD=ComputerCraftEdu.jar
[ ! -f $MOD ] && curl -s http://computercraftedu.com/downloads/ComputerCraftPlusComputerCraftEdu1.74.jar -o $MOD

LEVEL=computercraftedu_hour_of_code_1.7.10.3_166.zip
[ ! -f $LEVEL ] && curl -s https://worlds.education.minecraft.net/sites/default/files/worlds/166/world/$LEVEL -o $LEVEL

exit 0