#!/bin/bash

set -e

WORLD=/opt/server/world
if [ -z "$(ls -A $WORLD)" ]; then
	echo "New startup detected, unpacking level"
	cd $WORLD
	unzip /opt/computercraftedu_hour_of_code_1.7.10.3_166.zip
	cd "ComputerCraftEdu Hour of Code 1.7.10.2/" && mv * ..
fi